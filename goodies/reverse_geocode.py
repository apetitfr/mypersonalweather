from opencage.geocoder import OpenCageGeocode

k = '0ca19344ab056de9a82e845057a8f1ed'
geocoder = OpenCageGeocode(k)

filename = 'city_list_dash2.csv'
filename2 = 'city_list_out.csv'

in_file = open(filename, encoding='utf-8')
out_file = open(filename2, 'w', encoding='utf-8')

for line in in_file.readlines():
    coord = line.split(',')
    out_file.write(str(geocoder.reverse_geocode(coord[1], coord[2])))
    print ("Wrote: " + str(geocoder.reverse_geocode(coord[1], coord[2])))

in_file.close()
out_file.close()