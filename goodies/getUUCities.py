in_name = 'statecity.csv'
out_name = 'us_cities.csv'

in_file = open(in_name, encoding='utf-8')
out_file = open(out_name, 'w', encoding='utf-8')

for line in in_file.readlines():
    latlon = line.split(',')
    out_file.write(latlon[0] + ',' + latlon[1] + '-US,' + latlon[2] + ',' + latlon[3] + '\n')
    print('write: ' + latlon[0] + ',' + latlon[1] + '-US,' + latlon[2] + ',' + latlon[3])

in_file.close()
out_file.close()    
    
