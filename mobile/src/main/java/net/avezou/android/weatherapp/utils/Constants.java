package net.avezou.android.weatherapp.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apetitfr on 2/12/16.
 */
public class Constants {
    public static final String Current_URL = "http://api.openweathermap.org/data/2.5/weather?";
    public static final String Forecast_URL = "http://api.openweathermap.org/data/2.5/forecast?";
    public static final String APP_ID = "5f6323c02e0412b48f37034d70074161";
    public static final String SHARED_PREFS = "mow_shared_preferences";
    public static final String UNIT_PREF = "pref_key_units";
    public static final String DATE_PATTERN = "EEE, d MMM, HH:mm";
    public static final String DATE_PATTERN2 = "HH:mm:ss";

    public static final String LAT = "latitude";
    public static final String LON = "longitude";
    public static final long UPDATE_INTERVAL = 10800000;
    public static final int MY_PERMISSION_LOCATION = 5;
    public static final String FIRST_LAUNCH = "first_launch_pref";
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000*60*10;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static final String CITIES_FILENAME = "world_cities.csv";
    public static Map<String, String> CITIES_MAP = new HashMap<>(91850);
    public static String[] CITIES_LIST = new String[91850];
    public static final String CITIES_MAP_SERIALIZED = "world_cities_serialized_map.apf";
    public static final String CITIES_LIST_SERIALIZED = "world_cities_serialized_list.apf";
    public static final String TRACK_LOCATION_PREF = "track_location";
    public static final String CURRENT_CITY_PREF = "current_city";
    public static final String LOC_CHANGE_INTENT = "net.apetitfr.android.intent.broadcast.location";
}
