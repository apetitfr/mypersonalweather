package net.avezou.android.weatherapp.fragments.preferences;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import net.avezou.android.weatherapp.mypersonalweather.R;

/**
 * Created by apetitfr on 2/22/16.
 */
public class PersonalPrefFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.personal_prefs);
    }
}
