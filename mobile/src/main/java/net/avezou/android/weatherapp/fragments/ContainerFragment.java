package net.avezou.android.weatherapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by apetitfr on 2/11/16.
 */
public class ContainerFragment extends Fragment {
    protected String title;
    protected int page;
    protected Activity mActivity;

    // newInstance constructor for creating fragment with arguments
    public static Fragment newInstance(int page, String title) {
        Fragment fragmentContainer = null;
        switch (page) {
            case 0:
                fragmentContainer = new CurrentFragment();
                break;
            case 1:
                fragmentContainer = new ForecastFragment();
                break;
            case 2:
                fragmentContainer = new HistoryFragment();
                break;

        }
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentContainer.setArguments(args);
        return fragmentContainer;
    }
}
