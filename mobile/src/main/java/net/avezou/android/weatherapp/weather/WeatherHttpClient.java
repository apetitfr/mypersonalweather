package net.avezou.android.weatherapp.weather;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import net.avezou.android.weatherapp.utils.Constants;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/*
 * Copyright (C) 2013 Surviving with Android (http://www.survivingwithandroid.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class WeatherHttpClient {

    Activity mActivity;
    SharedPreferences mPrefs;
    String unit = "&units=imperial";

    public WeatherHttpClient(Activity activity){
        this.mActivity = activity;
        mPrefs = mActivity.getSharedPreferences(Constants.SHARED_PREFS, Activity.MODE_PRIVATE);

        if (mPrefs.getBoolean(Constants.UNIT_PREF, true)) {
            unit = "&units=imperial";
        } else {
            unit = "&units=metric";
        }
    }

    public String getWeatherData(String location) {
        HttpURLConnection con = null ;
        InputStream is = null;

        try {
            con = (HttpURLConnection) ( new URL(Constants.Current_URL + location + unit + "&appid=" + Constants.APP_ID)).openConnection();
            Log.d("NEGRED", "URL for the weather: " + Constants.Current_URL + location + unit + "&appid=" + Constants.APP_ID);
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();
            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;

    }


    public String getForecastData(String location) {
        HttpURLConnection con = null ;
        InputStream is = null;

        try {
            con = (HttpURLConnection) ( new URL(Constants.Current_URL + location + unit + "&appid=" + Constants.APP_ID)).openConnection();
            Log.d("NEGRED", "URL for the weather: " + Constants.Current_URL + location + unit + "&appid=" + Constants.APP_ID);
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();
            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;

    }

}