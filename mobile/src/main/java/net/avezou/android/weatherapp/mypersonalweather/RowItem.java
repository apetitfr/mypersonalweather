package net.avezou.android.weatherapp.mypersonalweather;

/**
 * Created by apetitfr on 2/22/16.
 */
public class RowItem {

    public int getmTemp() {
        return mTemp;
    }

    public void setmTemp(int mTemp) {
        this.mTemp = mTemp;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmDesc() {
        return mDesc;
    }

    public void setmDesc(String mDesc) {
        this.mDesc = mDesc;
    }

    public String getmIcon() {
        return mIcon;
    }

    public void setmIcon(String mIcon) {
        this.mIcon = mIcon;
    }

    int mTemp;
    String mDate, mDesc, mIcon;

    public RowItem(int temp, String date, String desc, String icon) {
        this.mDate = date;
        this.mDesc = desc;
        this.mIcon = icon;
        this.mTemp = temp;
    }
    
}
