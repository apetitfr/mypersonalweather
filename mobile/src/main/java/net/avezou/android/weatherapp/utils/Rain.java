package net.avezou.android.weatherapp.utils;

/**
 * Created by apetitfr on 2/12/16.
 */
public class Rain {
    private String time;
    private float ammount;
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public float getAmmount() {
        return ammount;
    }
    public void setAmmount(float ammount) {
        this.ammount = ammount;
    }
}
