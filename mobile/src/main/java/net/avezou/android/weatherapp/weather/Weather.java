package net.avezou.android.weatherapp.weather;

import net.avezou.android.weatherapp.utils.Clouds;
import net.avezou.android.weatherapp.utils.CurrentCondition;
import net.avezou.android.weatherapp.utils.Location;
import net.avezou.android.weatherapp.utils.Rain;
import net.avezou.android.weatherapp.utils.Snow;
import net.avezou.android.weatherapp.utils.Temperature;
import net.avezou.android.weatherapp.utils.Wind;

import org.json.JSONObject;

/**
 * Created by apetitfr on 2/12/16.
 */
public class Weather {
    public Location location;
    public CurrentCondition currentCondition = new CurrentCondition();
    public Temperature temperature = new Temperature();
    public Wind wind = new Wind();
    public Rain rain = new Rain();
    public Snow snow = new Snow()	;
    public Clouds clouds = new Clouds();
    public Long sunrise;
    public Long sunset;
    public Long time;

    public String iconData;
}
