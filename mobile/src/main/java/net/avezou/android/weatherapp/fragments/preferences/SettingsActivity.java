package net.avezou.android.weatherapp.fragments.preferences;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;

import net.avezou.android.weatherapp.mypersonalweather.R;
import net.avezou.android.weatherapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apetitfr on 2/22/16.
 */
public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onBuildHeaders(List<Header> target) {
        super.onBuildHeaders(target);
        loadHeadersFromResource(R.xml.preference_headers, target);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        ArrayList<Header> target = new ArrayList<>();
        loadHeadersFromResource(R.xml.preference_headers, target);
        for (Header h : target) {
            if (fragmentName.equals(h.fragment)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(Constants.UNIT_PREF)) {
            sharedPreferences.edit().putBoolean(key, sharedPreferences.getBoolean(key, true));
            Toast.makeText(getApplicationContext(), "" + sharedPreferences.getBoolean(key, true),
                                  Toast.LENGTH_SHORT).show();
            Log.d("NEGRED", "Pref changed: " + sharedPreferences.getBoolean(key, true));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getApplication().getSharedPreferences(Constants.SHARED_PREFS, Activity.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplication().getSharedPreferences(Constants.SHARED_PREFS, Activity.MODE_PRIVATE)
                .unregisterOnSharedPreferenceChangeListener(this);
    }
}
