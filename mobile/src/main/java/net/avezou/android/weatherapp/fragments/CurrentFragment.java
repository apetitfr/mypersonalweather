
package net.avezou.android.weatherapp.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.avezou.android.weatherapp.mypersonalweather.MainActivity;
import net.avezou.android.weatherapp.mypersonalweather.R;
import net.avezou.android.weatherapp.utils.Constants;
import net.avezou.android.weatherapp.utils.JSONWeatherParser;
import net.avezou.android.weatherapp.weather.Weather;
import net.avezou.android.weatherapp.weather.WeatherHttpClient;

import org.json.JSONException;

import java.text.SimpleDateFormat;

/**
 * Created by apetitfr on 2/11/16.
 */
public class CurrentFragment extends ContainerFragment {

    private TextView cityText;
    private TextView condDescr;
    private TextView temp;
    private TextView press;
    private TextView windSpeed;
    private TextView windDeg;

    private TextView hum;
    private TextView weatherIconView;
    private TextView time;
    private TextView sunrise;
    private TextView sunset;

    private SharedPreferences mPrefs;

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
        mActivity = getActivity();
        mPrefs = mActivity.getSharedPreferences(Constants.SHARED_PREFS, Activity.MODE_PRIVATE);

    }

    private BroadcastReceiver locationChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            JSONWeatherTask task = new JSONWeatherTask();
            String location = "lat=" + intent.getStringExtra("lat") +
                    "&lon=" + intent.getStringExtra("lon");
            Log.d("NEGRED", "Location: " + location);
            task.execute(new String[]{location});
        }
    };

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current, container, false);

        String location = "lat=" + mPrefs.getString(Constants.LAT, "43.4") +
                "&lon=" + mPrefs.getString(Constants.LON, "-77.4");
        Log.d("NEGRED", "LocationStr: " + location);

        cityText = (TextView) view.findViewById(R.id.cityText);
        condDescr = (TextView) view.findViewById(R.id.condDescr);
        temp = (TextView) view.findViewById(R.id.temp);
        hum = (TextView) view.findViewById(R.id.hum);
        press = (TextView) view.findViewById(R.id.press);
        windSpeed = (TextView) view.findViewById(R.id.windSpeed);
        windDeg = (TextView) view.findViewById(R.id.windDeg);
        weatherIconView = (TextView) view.findViewById(R.id.condIcon);
        time = (TextView)view.findViewById(R.id.currTime);
        sunrise = (TextView)view.findViewById(R.id.sunrise);
        sunset = (TextView)view.findViewById(R.id.sunset);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(
                locationChangeReceiver, new IntentFilter(Constants.LOC_CHANGE_INTENT)
        );

        JSONWeatherTask task = new JSONWeatherTask();
        task.execute(new String[]{location});
        
        return view;
    }

    private class JSONWeatherTask extends AsyncTask<String, Void, Weather> {

        @Override
        protected Weather doInBackground(String... params) {

            Weather weather = new Weather();
            try {
                WeatherHttpClient weatherHttpClient = new WeatherHttpClient(mActivity);
                String data = (weatherHttpClient.getWeatherData(params[0]));
                Log.d("NEGRED", "Data retrieved: " + data);


                Log.d("NEGRED", "data: " + data);
                weather = JSONWeatherParser.getWeather(data);

//                Log.d("NEGRED", "weather parsed: " + weather.iconData);
//
//                Log.d("NEGRED", "weather.iconData: " + weather.iconData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return weather;

        }

        @Override
        protected void onPostExecute(Weather weather) {
            super.onPostExecute(weather);

            if (weather == null) {
                Toast.makeText(getContext(), "Failed to get weather data",
                                      Toast.LENGTH_SHORT).show();
                return;

            }

            char degree = '\u00B0';
            String unit = ""+ degree + (mPrefs.getBoolean(Constants.UNIT_PREF, true) ? 'F' : 'C');

            if (weather.iconData != null) {
                //Bitmap img = BitmapFactory.decodeByteArray(weather.iconData, 0, weather.iconData.length);
                //weatherIconView.setImageBitmap(img);

                weatherIconView.setText(Html.fromHtml(weather.iconData));
                weatherIconView.setTypeface(MainActivity.weatherFont);
//                weatherIconView.setText(weather.iconData);
            }

            cityText.setText(weather.location.getCity() + "," + weather.location.getCountry());
            condDescr.setText(weather.currentCondition.getDescr());
            temp.setText("" + Math.round((weather.temperature.getTemp())) + unit);
            hum.setText("" + weather.currentCondition.getHumidity() + "%");
            if (mPrefs.getBoolean(Constants.UNIT_PREF, true)) {
                press.setText("" + Math.round(weather.currentCondition.getPressure() * 0.02961339710085) + " in");
                windSpeed.setText(weather.wind.getSpeed() + " mph");
            } else {
                press.setText(weather.currentCondition.getPressure() + " hPa");
                windSpeed.setText(weather.wind.getSpeed() + " m/s");
            }

            windDeg.setText("" + getDirection(Math.round(weather.wind.getDeg())));

            SimpleDateFormat timeFormat = new SimpleDateFormat(Constants.DATE_PATTERN);
            SimpleDateFormat timeFormat2 = new SimpleDateFormat(Constants.DATE_PATTERN2);
            String formattedTime = timeFormat.format(weather.time);
            String formattedSunrise = timeFormat2.format(weather.sunrise);
            String formattedSunset = timeFormat2.format(weather.sunset);
            time.setText(formattedTime);
            sunrise.setText(formattedSunrise);
            sunset.setText(formattedSunset);


        }

        public String getDirection(float deg) {
            if ((deg >=348.75 && deg <= 360.0) || (deg < 11.25 && deg >= 0.0)) {
                return "N";
            } else if (deg >= 11.25 && deg < 33.75) {
                return "NNE";
            }else if (deg >= 33.75 && deg < 56.25) {
                return "NE";
            } else if (deg >= 56.25 && deg < 78.75) {
                return "ENE";
            }
            else if (deg >= 78.75 && deg < 101.25) {
                return "E";
            } else if (deg >= 101.25 && deg < 123.75) {
                return "ESE";
            } else if (deg >= 123.75 && deg < 146.25) {
                return "SE";
            } else if (deg >= 146.25 && deg < 168.75) {
                return "SSE";
            } else if (deg >= 168.75 && deg < 191.25) {
                return "S";
            } else if (deg >= 191.25 && deg < 213.75) {
                return "SSW";
            } else if (deg >= 213.75 && deg < 236.25) {
                return "SW";
            } else if (deg >= 236.25 && deg < 258.75) {
                return "WSW";
            } else if (deg >= 258.75 && deg < 281.25) {
                return "W";
            } else if (deg >= 281.25 && deg < 303.75) {
                return "WNW";
            } else if (deg >= 303.75 && deg < 326.25) {
                return "NW";
            } else if (deg >= 326.25 && deg < 348.75) {
                return "NNW";
            } else {
                return "UNKNOWN";
            }

        }

    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(
                locationChangeReceiver
        );
        super.onDestroy();
    }
}

