package net.avezou.android.weatherapp.mypersonalweather;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ToggleButton;

import com.appyvet.rangebar.RangeBar;

import net.avezou.android.weatherapp.utils.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.HashMap;

/**
 * Created by apetitfr on 2/17/16.
 */
public class PopupActivity extends AppCompatActivity {

    AutoCompleteTextView mAutoCompleteTextView;
    private static final String LOG_TAG = "MPW-" + PopupActivity.class.getName();
    private String cityState;
    Button submit, cancel;
    ToggleButton trackLocationToggleButton, unitPrefToggleButton;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    RangeBar rangeBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getSharedPreferences(Constants.SHARED_PREFS, Activity.MODE_PRIVATE);
        editor = prefs.edit();

        final Intent intent = new Intent(this, MainActivity.class);
        if (!prefs.getBoolean(Constants.FIRST_LAUNCH, true)) {
            startActivity(intent);
            finish();
        }

        setContentView(R.layout.activity_popup);
        mAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.city_search_text);
        trackLocationToggleButton = (ToggleButton) findViewById(R.id.track_location);
        unitPrefToggleButton = (ToggleButton) findViewById(R.id.units);
        rangeBar = (RangeBar) findViewById(R.id.personal_range);
        cancel = (Button) findViewById(R.id.dialog_cancel_button);
        submit = (Button) findViewById(R.id.dialog_submit_button);

        try {
            copyFileFromAssets();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mAutoCompleteTextView.setThreshold(3);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                                                                       android.R.layout.simple_dropdown_item_1line, Constants.CITIES_LIST);
        mAutoCompleteTextView.setAdapter(adapter);

        trackLocationToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trackLocationToggleButton.isChecked()) {
                    submit.setEnabled(true);
                } else if (Constants.CITIES_MAP.get(mAutoCompleteTextView.getText().toString()) != null) {
                    submit.setEnabled(true);
                } else {
                    submit.setEnabled(false);
                }
            }
        });
        unitPrefToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unitPrefToggleButton.isChecked()) {
                    rangeBar.setTickStart(32);
                    rangeBar.setTickEnd(104);
                    rangeBar.setTickInterval(3);
                } else {
                    rangeBar.setTickStart(0);
                    rangeBar.setTickEnd(40);
                    rangeBar.setTickInterval(2);
                }
            }
        });
        mAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(LOG_TAG, "clicked:" + parent.getItemAtPosition(position).toString());
                cityState = parent.getItemAtPosition(position).toString();
                if (cityState != null) {
                    submit.setEnabled(true);
                } else if (trackLocationToggleButton.isChecked()) {
                    submit.setEnabled(true);
                } else {
                    submit.setEnabled(true);
                }
                mAutoCompleteTextView.forceLayout();
            }
        });


        rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue,
                                              String rightPinValue) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putBoolean(Constants.FIRST_LAUNCH, false).commit();
                editor.putBoolean(Constants.TRACK_LOCATION_PREF, trackLocationToggleButton.isChecked());
                editor.putBoolean(Constants.UNIT_PREF, unitPrefToggleButton.isChecked());
                editor.putString(Constants.CURRENT_CITY_PREF, mAutoCompleteTextView.getText().toString());
                serializeAndSaveData();
                if (cityState != null) {
                    String[] latLon = Constants.CITIES_MAP.get(cityState).split(",");
                    editor.putString(Constants.LAT, latLon[0]);
                    editor.putString(Constants.LON, latLon[1]);
                }
                Constants.CITIES_LIST = new String[0];
                Constants.CITIES_MAP = new HashMap<String, String>();
                editor.commit();
                startActivity(intent);
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * Copy the list of cities file from assets to internal storage.
     *
     * @throws IOException
     */
    private void copyFileFromAssets() throws IOException {
        String outputFilename = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Constants.CITIES_FILENAME;
        OutputStream outputStream = new FileOutputStream(outputFilename);
        InputStream inputStream = getAssets().open("data" + File.separator + Constants.CITIES_FILENAME);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }
        inputStream.close();
        outputStream.flush();
        outputStream.close();
        readCitiesFromFile(outputFilename);
        Log.d("NEGRED", "file: " + outputFilename);
    }

    /**
     * Read the list of cities from internal storage to an array and map
     *
     * @param inputFilename
     */
    private void readCitiesFromFile(String inputFilename) {
        InputStream internalStorageInputStream = null;
        try {
            // open the file for reading
            internalStorageInputStream = new FileInputStream(inputFilename);

            // if file the available for reading
            if (internalStorageInputStream != null) {
                // prepare the file for reading
                InputStreamReader inputStreamReader = new InputStreamReader(internalStorageInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line;

                // read every line of the file into the line-variable, on line at the time
                int i = 0;
                do {
                    line = bufferedReader.readLine();
                    if (line != null) {
                        String[] splitLine = line.split(",");
                        // ['City', '<State->Country', 'Lat', 'Lon']
                        Constants.CITIES_MAP.put(splitLine[0] + "," + splitLine[1], splitLine[2] + "," + splitLine[3]);
                        Constants.CITIES_LIST[i] = splitLine[0] + "," + splitLine[1];
                        i++;
                    }
                } while (line != null);

            }
        } catch (Exception ex) {
            // print stack trace.
        } finally {
            // close the file.
            try {
                internalStorageInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            (new File(inputFilename)).delete();

        }

    }

    /**
     * Serialize cities list and map and save to storage Also clears data from array and map, they
     * should not be needed for this session
     */
    private void serializeAndSaveData() {
        try {
            FileOutputStream mapOutputStream = new FileOutputStream(Constants.CITIES_MAP_SERIALIZED);
            FileOutputStream listOutputStream = new FileOutputStream(Constants.CITIES_LIST_SERIALIZED);
            ObjectOutputStream mapObjectStream = new ObjectOutputStream(mapOutputStream);
            ObjectOutputStream listObjectStream = new ObjectOutputStream(listOutputStream);
            mapObjectStream.writeObject(Constants.CITIES_MAP);
            listObjectStream.writeObject(Constants.CITIES_LIST);
            Log.d("NEGRED", "Serialized size: " + (new File(listOutputStream.toString())).length());
            mapObjectStream.close();
            listObjectStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}


