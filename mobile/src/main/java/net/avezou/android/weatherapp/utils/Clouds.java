package net.avezou.android.weatherapp.utils;

/**
 * Created by apetitfr on 2/12/16.
 */
public class Clouds {
    private int perc;

    public int getPerc() {
        return perc;
    }

    public void setPerc(int perc) {
        this.perc = perc;
    }
}
