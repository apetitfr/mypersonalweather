package net.avezou.android.weatherapp.fragments.preferences;

import android.content.Context;
import android.os.Build;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import net.avezou.android.weatherapp.utils.Constants;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by apetitfr on 2/22/16.
 */
public class AutoCompletePreference extends EditTextPreference {

    private static final String LOG_TAG = "MPW-" + AutoCompletePreference.class.getName();
    private static AutoCompleteTextView mEditText = null;

    public AutoCompletePreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readSerializedData();
    }

    public AutoCompletePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        readSerializedData();
        mEditText = new AutoCompleteTextView(context, attrs);
        mEditText.setThreshold(3);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, Constants.CITIES_LIST);
        mEditText.setAdapter(adapter);
        mEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // get the lat lon from the map here
            }
        });
    }

    public AutoCompletePreference(Context context) {
        super(context);
        readSerializedData();
    }

    /**
     * Read cities array and map data from serialized file
     */
    public static void readSerializedData() {
        try {
            FileInputStream mapInputStream = new FileInputStream(Constants.CITIES_MAP_SERIALIZED);
            FileInputStream listInputStream = new FileInputStream((Constants.CITIES_LIST_SERIALIZED));
            ObjectInputStream mapObjectStream = new ObjectInputStream(mapInputStream);
            ObjectInputStream listObjectStream = new ObjectInputStream(listInputStream);
            Constants.CITIES_MAP = (Map) mapObjectStream.readObject();
            Constants.CITIES_LIST = (String[]) listObjectStream.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        AutoCompleteTextView editText = mEditText;
        editText.setText(getText());

        ViewParent oldParent = editText.getParent();
        if (oldParent != view) {
            if (oldParent != null) {
                ((ViewGroup) oldParent).removeView(editText);
            }
            onAddEditTextToDialogView(view, editText);
        }
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            String value = mEditText.getText().toString();
            if (callChangeListener(value)) {
                setText(value);
            }
        }

        // on dialog closed also delete the array and map for space save
        Constants.CITIES_LIST = new String[]{};
        Constants.CITIES_MAP = new HashMap<String, String>();
    }
}
