package net.avezou.android.weatherapp.mypersonalweather;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by apetitfr on 2/22/16.
 */
public class ForecastAdapter extends BaseAdapter {

    List<RowItem> rowItemList;
    Context context;

    TextView tempView, iconView, dateView, descView;

    public ForecastAdapter(Context ctx, List<RowItem> rowItems) {
        this.rowItemList = rowItems;
        this.context = ctx;
    }
    
    @Override
    public int getCount() {
        return rowItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItemList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                                                                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.forecast_listitem, null);
        }

        tempView = (TextView)convertView.findViewById(R.id.ftemp);
        iconView = (TextView)convertView.findViewById(R.id.ficon);
        descView = (TextView)convertView.findViewById(R.id.fdesc);
        dateView = (TextView)convertView.findViewById(R.id.fdate);

        RowItem item = rowItemList.get(position);

        tempView.setText(item.getmTemp() +"");
        iconView.setText(item.getmIcon());
        descView.setText(item.getmDesc());
        dateView.setText(item.getmDate());

        return convertView;
    }
}
