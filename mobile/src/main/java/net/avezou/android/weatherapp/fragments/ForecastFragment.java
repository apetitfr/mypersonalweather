package net.avezou.android.weatherapp.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import net.avezou.android.weatherapp.mypersonalweather.ForecastAdapter;
import net.avezou.android.weatherapp.mypersonalweather.MainActivity;
import net.avezou.android.weatherapp.mypersonalweather.R;
import net.avezou.android.weatherapp.mypersonalweather.RowItem;
import net.avezou.android.weatherapp.utils.Constants;
import net.avezou.android.weatherapp.utils.JSONWeatherParser;
import net.avezou.android.weatherapp.weather.Weather;
import net.avezou.android.weatherapp.weather.WeatherHttpClient;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by apetitfr on 2/11/16.
 */
public class ForecastFragment extends ContainerFragment {

    ListView forecastList;
    ForecastAdapter forecastAdapter;
    List<RowItem> rowItems = new ArrayList<RowItem>();
    SharedPreferences mPrefs;


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 1);
        title = getArguments().getString("someTitle");
        mActivity = getActivity();
        mPrefs = getActivity().getSharedPreferences(Constants.SHARED_PREFS, Activity.MODE_PRIVATE);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);

        forecastList = (ListView)view.findViewById(R.id.forecast_listview);
        forecastAdapter = new ForecastAdapter(getActivity(), rowItems);
        rowItems.add(new RowItem(56, "2016-12-12", "meatballs", "04d"));
        forecastList.setAdapter(forecastAdapter);

        forecastList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity().getApplicationContext(), rowItems.get(position) + " clicked",
                                      Toast.LENGTH_SHORT).show();
            }
        });

        String location = "lat=" + mPrefs.getString(Constants.LAT, "43.4") +
                                  "&lon=" + mPrefs.getString(Constants.LON, "-77.4");

        JSONForecastTask task = new JSONForecastTask();
        task.execute(new String[]{location});

        return view;
    }


    private class JSONForecastTask extends AsyncTask<String, Void, Weather> {
        @Override
        protected Weather doInBackground(String... params) {

            Weather weather = new Weather();
            try {
                WeatherHttpClient weatherHttpClient = new WeatherHttpClient(mActivity);
                String data = (weatherHttpClient.getForecastData(params[0]));
                Log.d("NEGRED", "Data retrieved: " + data);


                Log.d("NEGRED", "data: " + data);
                weather = JSONWeatherParser.getWeather(data);

                //                Log.d("NEGRED", "weather parsed: " + weather.iconData);
                //
                //                Log.d("NEGRED", "weather.iconData: " + weather.iconData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return weather;

        }

        @Override
        protected void onPostExecute(Weather weather) {
            super.onPostExecute(weather);

            if (weather == null) {
                Toast.makeText(getContext(), "Failed to get weather data",
                                      Toast.LENGTH_SHORT).show();
                return;

            }

            char degree = '\u00B0';
            String unit = ""+ degree + (mPrefs.getBoolean(Constants.UNIT_PREF, true) ? 'F' : 'C');

            Log.d("NEGRED", "Got some data: " + weather.toString());

//            if (weather.iconData != null) {
//                //Bitmap img = BitmapFactory.decodeByteArray(weather.iconData, 0, weather.iconData.length);
//                //weatherIconView.setImageBitmap(img);
//
//                weatherIconView.setText(Html.fromHtml(weather.iconData));
//                weatherIconView.setTypeface(MainActivity.weatherFont);
//                //                weatherIconView.setText(weather.iconData);
//            }
//
//            cityText.setText(weather.location.getCity() + "," + weather.location.getCountry());
//            condDescr.setText(weather.currentCondition.getDescr());
//            temp.setText("" + Math.round((weather.temperature.getTemp())) + unit);
//            hum.setText("" + weather.currentCondition.getHumidity() + "%");
//            if (mPrefs.getBoolean(Constants.UNIT_PREF, true)) {
//                press.setText("" + Math.round(weather.currentCondition.getPressure() * 0.02961339710085) + " in");
//                windSpeed.setText(weather.wind.getSpeed() + " mph");
//            } else {
//                press.setText(weather.currentCondition.getPressure() + " hPa");
//                windSpeed.setText(weather.wind.getSpeed() + " m/s");
//            }
//
//            windDeg.setText("" + getDirection(Math.round(weather.wind.getDeg())));
//
//            SimpleDateFormat timeFormat = new SimpleDateFormat(Constants.DATE_PATTERN);
//            SimpleDateFormat timeFormat2 = new SimpleDateFormat(Constants.DATE_PATTERN2);
//            String formattedTime = timeFormat.format(weather.time);
//            String formattedSunrise = timeFormat2.format(weather.sunrise);
//            String formattedSunset = timeFormat2.format(weather.sunset);
//            time.setText(formattedTime);
//            sunrise.setText(formattedSunrise);
//            sunset.setText(formattedSunset);


        }
    }
}

